import axios from 'axios';
import { API } from './constants';

class HTTPConnection {
  constructor() {
    const headers = {
      access_token: 'foobar'
    };

    const opts = {
      baseURL: API,
      timeout: 10000,
      headers
    };

    this.instance = axios.create(opts);
  }
}

export default HTTPConnection;
