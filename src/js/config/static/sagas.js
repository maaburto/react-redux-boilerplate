import { all } from 'redux-saga/effects';
// import Item from '../../Item/static/sagas';

function* initSaga() {
  yield true;
}

// notice how we now only export the rootSaga
// single entry point to start all Sagas at once
export default function* rootSaga() {
  yield all(initSaga);
  // yield all(Item);
}
