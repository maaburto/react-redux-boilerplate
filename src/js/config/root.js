import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import Loadable from 'react-loadable';
import RouteWithSubRoutes from './routes';
import item from '../Item/route';

const AllRoutes = [...item];

const App = Loadable({
  loader: () => import(/* webpackChunkName: "App" */ '../app.jsx'),
  loading: () => <div>Loading...</div>
});

const NoMatch = Loadable({
  loader: () => import(/* webpackChunkName: "NoMatch" */ '../Utils/404/noMatch.jsx'),
  loading: () => <div>Loading...</div>
});

const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={App} />
        {AllRoutes.map((route, i) => (
          <RouteWithSubRoutes key={i.toString()} {...route} />
        ))}
        <Route component={NoMatch} />
      </Switch>
    </Router>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object
};

Root.defaultProps = {
  store: {}
};

export default Root;
