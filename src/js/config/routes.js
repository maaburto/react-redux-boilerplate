import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

// wrap <Route> and use this everywhere instead, then when
// sub routes are added to any route it'll work
const RouteWithSubRoutes = ({ exact, path, routes, component: Component }) => {
  return (
    <Route
      exact={exact}
      path={path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <Component {...props} routes={routes} />
      )}
    />
  );
};

RouteWithSubRoutes.propTypes = {
  exact: PropTypes.bool.isRequired,
  path: PropTypes.string.isRequired,
  routes: PropTypes.array,
  component: PropTypes.func.isRequired
};

RouteWithSubRoutes.defaultProps = {
  routes: []
};

export default RouteWithSubRoutes;
