// runSaga is middleware.run function
// rootSaga is a your root saga for static sagas
class SagaManager {
  create(runSaga, rootSaga) {
    // Create a dictionary to keep track of injected sagas
    this.runSaga = runSaga;
    this.injectedSagas = new Map();

    // Inject the root saga as it a statically loaded file,
    this.inject('root', rootSaga);

    return this.inject;
  }

  inject(key, saga) {
    const isInjected = _key => this.injectedSagas.has(_key);

    // We won't run saga if it is already injected
    if (isInjected(key)) return;

    // Sagas return task when they executed, which can be used
    // to cancel them
    const task = this.runSaga(saga);

    // Save the task if we want to cancel it in the future
    this.injectedSagas.set(key, task);
  }
}

export default SagaManager;
