import React from 'react';
import { render } from 'react-dom';
import configureStore from './config/store';
import Root from './config/root';

const store = configureStore(); // You can also pass in an initialState here

render(<Root store={store} />, document.getElementById('app'));
