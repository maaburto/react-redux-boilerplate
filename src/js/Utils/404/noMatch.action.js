function noURLMatch(noMatch) {
  return {
    type: 'NO_MATCH_DATA_URL',
    noMatch
  };
}

export default noURLMatch;
