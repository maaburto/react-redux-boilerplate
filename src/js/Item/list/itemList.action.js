import { put, takeLatest } from 'redux-saga/effects';
import HTTP from '../../config/http';

function itemsHasErrored(bool) {
  return {
    type: 'ITEMS_HAS_ERRORED',
    hasErrored: bool
  };
}

function itemsIsLoading(bool) {
  return {
    type: 'ITEMS_IS_LOADING',
    isLoading: bool
  };
}

function itemsFetchDataSuccess(items) {
  return {
    type: 'ITEMS_FETCH_DATA_SUCCESS',
    items
  };
}

function* itemsFetchData() {
  const http = new HTTP();
  const db = http.instance;

  yield put(itemsIsLoading(true));

  try {
    const res = yield db.get('/items');

    yield put(itemsIsLoading(false));
    yield put(itemsFetchDataSuccess(res.data));
  } catch (err) {
    yield put(itemsHasErrored(true));
  }
}

// Watchers Saga
function* watchItemsFetchData() {
  yield takeLatest('ITEMS_REQUEST', itemsFetchData);
}

export default watchItemsFetchData;
