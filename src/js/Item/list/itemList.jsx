import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';

import '../style.scss';

const mapStateToProps = state => {
  return {
    items: state.items,
    hasErrored: state.itemsHasErrored,
    isLoading: state.itemsIsLoading
  };
};

class ItemList extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    items: PropTypes.array,
    hasErrored: PropTypes.bool,
    isLoading: PropTypes.bool
  };

  static defaultProps = {
    dispatch: () => {},
    items: [],
    hasErrored: false,
    isLoading: false
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'ITEMS_REQUEST' });
  }

  render() {
    const { items, hasErrored, isLoading } = this.props;

    if (hasErrored) {
      return <p>Sorry! There was an error loading the items</p>;
    }

    if (isLoading) {
      return <p>Loading…</p>;
    }

    return (
      <>
        <h1 className="insane">Item List Available</h1>
        <ul>
          {items.map(item => (
            <li key={item.id}>
              <Link to={{ pathname: `/items/${item.id}` }}>{item.label}</Link>
            </li>
          ))}
        </ul>
      </>
    );
  }
}

export default withRouter(connect(mapStateToProps, null)(ItemList));
