const initialState = {
  items: [],
  isLoading: false,
  hasErrored: false,
  request: false
};

function itemsRequest(state = initialState.request, action) {
  switch (action.type) {
    case 'ITEMS_REQUEST':
      return true;
    default:
      return state;
  }
}

function itemsHasErrored(state = initialState.hasErrored, action) {
  switch (action.type) {
    case 'ITEMS_HAS_ERRORED':
      return action.hasErrored;
    default:
      return state;
  }
}

function itemsIsLoading(state = initialState.isLoading, action) {
  switch (action.type) {
    case 'ITEMS_IS_LOADING':
      return action.isLoading;
    default:
      return state;
  }
}

function items(state = initialState.items, action) {
  switch (action.type) {
    case 'ITEMS_FETCH_DATA_SUCCESS':
      return action.items;
    default:
      return state;
  }
}

export { items, itemsRequest, itemsHasErrored, itemsIsLoading };
