import { ItemList, Itemform } from './module';

const items = 'items';

const routes = [
  {
    path: `/${items}`,
    component: ItemList,
    exact: true
  },
  {
    path: `/${items}/:itemId`,
    component: Itemform,
    exact: false
  }
];

export default routes;
