import React from 'react';
import Loadable from 'react-loadable';
import configureStore from '../config/store';

import { itemListSagas, itemFormSagas } from './lazy/sagas';
import { itemListReducers, itemFormReducers } from './lazy/reducers';

const { injectReducer, sagaManager } = configureStore();

const ItemList = Loadable({
  loader: () =>
    import(/* webpackChunkName: "ItemList" */ './list/itemList.jsx').then(module => {
      itemListReducers(injectReducer);
      itemListSagas(sagaManager);

      return module;
    }),
  loading: () => <div>Loading...</div>
});

const Itemform = Loadable({
  loader: () =>
    import(/* webpackChunkName: "Itemform" */ './form/itemForm.jsx').then(module => {
      itemFormReducers(injectReducer);
      itemFormSagas(sagaManager);

      return module;
    }),
  loading: () => <div>Loading...</div>
});

export { Itemform, ItemList };
