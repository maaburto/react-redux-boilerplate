import { put, takeLatest } from 'redux-saga/effects';
import HTTP from '../../config/http';

function itemHasErrored(bool) {
  return {
    type: 'ITEM_HAS_ERRORED',
    hasErrored: bool
  };
}

function itemIsLoading(bool) {
  return {
    type: 'ITEM_IS_LOADING',
    isLoading: bool
  };
}

function itemFetchDataSuccess(item) {
  return {
    type: 'ITEM_FETCH_DATA_SUCCESS',
    item
  };
}

function itemFetchDataEmpty(item) {
  return {
    type: 'ITEM_FETCH_DATA_EMPTY',
    item
  };
}

// function errorAfterFiveSeconds() {
//   // We return a function instead of an action object
//   return (dispatch) => {
//     setTimeout(() => {
//       // This function is able to dispatch other action creators
//       dispatch(itemHasErrored(true));
//     }, 5000);
//   };
// }

function* itemFetchData(action) {
  const { itemId } = action;
  const http = new HTTP();
  const db = http.instance;

  yield put(itemIsLoading(true));
  let res = null;

  try {
    res = yield db.get(`/items/${itemId}`);

    yield put(itemIsLoading(false));
    if (res.data.id) {
      yield put(itemFetchDataSuccess(res.data));
    }
  } catch (err) {
    if (err.response.status === 500) {
      yield put(itemHasErrored(true));
    } else {
      res = err.response;
    }
  } finally {
    if (res.status === 404) {
      yield put(itemFetchDataEmpty(res.data));
      yield put(itemIsLoading(false));
    }
  }
}

// Watchers Saga
function* watchItemFetchData() {
  yield takeLatest('ITEM_REQUEST', itemFetchData);
}

export default watchItemFetchData;
