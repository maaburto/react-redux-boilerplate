const webpack = require('webpack');
const { constants } = require('./webpack.config');
const path = require('path');

const entries = ['./src/js/index.js', './src/styles/main.scss'];

const outputs = {
  publicPath: '/',
  filename: 'js/[name].[hash].js',
  chunkFilename: 'js/[name].[hash].js',
  path: path.resolve(__dirname, 'dist')
};

const modules = {
  rules: [{
    test: /\.jsx?$/,
    exclude: [/node_modules/],
    use: ['babel-loader']
  }, {
    enforce: 'pre',
    test: /\.(js|jsx)$/,
    exclude: [/node_modules/],
    loaders: 'eslint-loader'
  }]
};

const plugins = [new webpack.DefinePlugin(constants)];

module.exports = {
  entry: entries,
  output: outputs,
  module: modules,
  plugins: plugins
};
